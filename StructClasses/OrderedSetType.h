#pragma once

#include <iostream>
#include <malloc.h>

class OrderedSetType
{
	private:
		int *vector, size;

	public:
		OrderedSetType();

		OrderedSetType(int);

		OrderedSetType(const OrderedSetType& orderedSetType);

		bool insertElement(int element);

		bool eraseElement(int element);
		
		void read();

		void clear();

		void print();

		int find(int element);

		~OrderedSetType();
};