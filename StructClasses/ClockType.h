#pragma once

class ClockType
{
	private:
		int sec, min, hr;

	public:

		ClockType();

		ClockType(int sec, int min, int hr);

		void setTime(int, int, int);

		void geTime(int&, int&, int&) const;

		void printTime() const;

		int incrementSeconds();

		int incrementMinutes();

		int incrementHours();

		bool equalTime(const ClockType& clockType) const;

		~ClockType();
};