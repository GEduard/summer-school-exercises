#include <iostream>

bool PalindromWord(char* word)
{
	int count = 0;
	bool OK = true;
	char* check = word;
	while (*check != '\0')
	{
		check++;
		count++;
	}
	for (size_t i = 0; i < count; i++)
	{
		if (*(word + i) != *(check - i - 1))
		{
			OK = false;
			break;
		}
	}
	return OK;
}

int main()
{
	if (PalindromWord("ANNA"))
		std::cout << "True!" << std::endl;
	else
		std::cout << "False!" << std::endl;
	return 0;
}