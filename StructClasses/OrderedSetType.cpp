#include "OrderedSetType.h"

OrderedSetType::OrderedSetType() {}

OrderedSetType::OrderedSetType(int size)
{
	this->size = size;
	vector = static_cast<int*>(malloc(this->size * sizeof(int)));
}

OrderedSetType::OrderedSetType(const OrderedSetType & orderedSetType)
{
	this->size = orderedSetType.size;
	vector = static_cast<int*>(malloc(size * sizeof(int)));
	for (size_t i = 0; i < size; i++)
	{
		this->vector[i] = orderedSetType.vector[i];
	}
}

bool OrderedSetType::insertElement(int element)
{
	if (find(element) == -1)
	{
		int position = -1;
		for (size_t i = 0; i < size - 1; i++)
		{
			if (vector[i] < element && vector[i + 1] > element)
			{
				position = i + 1;
				break;
			}
		}
		if (position == -1)
		{
			position = size;
		}
		vector = static_cast<int*>(realloc(vector, ++size));
		for (size_t i = size - 1; i > position; i--)
			vector[i] = vector[i - 1];
		vector[position] = element;
		return true;
	}
	std::cout << "Elementul nu a fost inserat." << std::endl;
	return false;
}

bool OrderedSetType::eraseElement(int element)
{
	if (find(element) != -1)
	{
		for (size_t i = find(element); i < size; i++)
			vector[i] = vector[i + 1];
		size--;
		vector = static_cast<int*>(realloc(vector, size));
		return true;
	}
	std::cout << "Elementul nu exista in sir!" << std::endl;
	return false;
}

void OrderedSetType::clear()
{
	free(vector);
	size = 0;
}

void OrderedSetType::read()
{
	std::cout << "Introduceti elementele vectorului: ";
	for (size_t i = 0; i < size; i++)
		std::cin >> vector[i];
}

void OrderedSetType::print()
{
	std::cout << "Elementele vectorului sunt: ";
	for (size_t i = 0; i < size; i++)
		std::cout << vector[i] << " ";
	std::cout << std::endl;
}

int OrderedSetType::find(int element)
{
	int st = 0;
	int dr = this->size - 1;

	if (st >= dr)
		return -1;

	while (st < dr)
	{
		int mij = (st + dr) / 2;
		if (this->vector[mij] == element)
			return mij;
		if (vector[st] > element || vector[dr] < element)
		{
			return -1;
		}
		if (this->vector[mij] > element)
		{
			dr = mij;
		}
		else
		{
			st = mij;
		}
	}
	return st;
}

OrderedSetType::~OrderedSetType() 
{
	size = 0;
	free(vector);
}
