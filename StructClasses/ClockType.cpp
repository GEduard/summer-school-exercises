#include <iostream>
#include "ClockType.h"

ClockType::ClockType() {}

ClockType::ClockType(int sec, int min, int hr)
{
	this->sec = sec;
	this->min = min;
	this->hr = hr;
}

void ClockType::setTime(int sec, int min, int hr)
{
	if (hr >= 0 && hr <= 23 && min >= 0 && min <= 59 && sec >= 0 && sec <= 59)
	{
		this->sec = sec;
		this->min = min;
		this->hr = hr;
	}
	else
		throw "Invalid time format!";
}

void ClockType::geTime(int &sec, int &min, int &hr) const
{
	sec = this->sec;
	min = this->min;
	hr = this->hr;
}

void ClockType::printTime() const
{
	std::cout << "Time: ";

	if (this->hr < 10)
		std::cout << "0" << this->hr << ":";
	else
		std::cout << this->hr << ":";

	if (this->min < 10)
		std::cout << "0" << this->min << ":";
	else
		std::cout << this->min << ":";

	if (this->sec < 10)
		std::cout << "0" << this->sec << std::endl;
	else
		std::cout << this->sec << std::endl;
}

int ClockType::incrementSeconds()
{
	sec++;
	if (sec == 60)
	{
		sec = 0;
		incrementMinutes();
	}
	return sec;
}

int ClockType::incrementMinutes()
{
	min++;
	if (min == 60)
	{
		min = 0;
		incrementHours();
	}
	return min;
}

int ClockType::incrementHours()
{
	hr++;
	if (hr == 24) hr = 0;
	return hr;
}

bool ClockType::equalTime(const ClockType& clockType) const
{
	return (this->sec == clockType.sec && this->min == clockType.min && this->hr == clockType.hr);
}

ClockType::~ClockType() {}
