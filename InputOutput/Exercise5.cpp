#include <fstream>
#include <iostream>

int main()
{
	int num1, num2;
	std::ifstream infile;
	std::ofstream outfile;
	infile.open("Input.dat");
	outfile.open("Output.dat");
	infile >> num1 >> num2;
	outfile << "Sum = " << num1 + num2 << std::endl;
	infile.close();
	outfile.close();
	return 0;
}
