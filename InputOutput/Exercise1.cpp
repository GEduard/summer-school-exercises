#include <iomanip>
#include <iostream>

int main()
{
	float *a;
	int n, colIndex = 0, rowIndex = 0;
	std::cout << "Introduceti dimensiunea matricii:";
	std::cin >> n;
	a = new float[n * n];
	std::cout << "Introduceti elementele:" << std::endl << std::endl;
	for (size_t i = 0; i < n * n; i++)
	{
		std::cin >> a[i];
	}
	for (size_t i = 0; i < n; i++)
		std::cout << std::setfill('.') << std::setw(10) << "[" << i << "]";
	std::cout << std::endl;
	for (size_t i = 0; i < n * n; i++)
	{
		if (colIndex == 0)
		{
			std::cout << "[" << rowIndex << "]";
			rowIndex++;
		}

		if (a[i] != 0)
			std::cout << std::setfill('.') << std::setw(10 + colIndex) << std::setprecision(3) << a[i];
		else
			std::cout << std::setfill('.') << std::setw(10 + colIndex) << "";

		if (colIndex == n - 1)
		{
			colIndex = -1;
			std::cout << std::endl;
		}

		colIndex++;
	}
	std::cout << std::endl;
	delete a;
	return 0;
}