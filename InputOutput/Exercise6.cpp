#include <string>
#include <fstream>
#include <iostream>

void printLines(int index, std::string szFileName)
{
	int countLine = 0, substract = -1;
	std::ifstream file;
	file.open(szFileName);
	if (file.is_open())
	{
		char szTextLine[64];
		while (countLine < index)
		{
			if (file.get() != '\n')
			{
				file.seekg(substract, file.end);
				substract--;
			}
			else
			{
				countLine++;
				substract--;
			}
		}
		while (file.getline(szTextLine, 64))
			std::cout << szTextLine << std::endl;
		file.close();
	}
	else
	{
		std::cout << "ERROR: Couldn't open the specified file!" << std::endl;
	}
}

int main()
{
	int index;
	std::string szFileName;
	std::cout << "Enter the number of lines:";
	std::cin >> index;
	std::cout << "Enter the file name:";
	std::cin >> szFileName;
	printLines(index, szFileName);
	return 0;
}