#include <string>
#include <fstream>
#include <iostream>

int main()
{
	std::cout << "Input file name:" << std::endl;
	std::string szFileName;
	std::cin >> szFileName;
	std::size_t found = szFileName.find(".txt");
	if (found != std::string::npos)
	{
		std::cout << "File name to open: " << szFileName << std::endl;

		std::ifstream file;
		file.open(szFileName);

		char szTextLine[64];
		while (file.getline(szTextLine, 64))
			std::cout << ">" << szTextLine << std::endl;
		file.close();
	}
	return 0;
}