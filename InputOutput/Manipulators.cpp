#define _USE_MATH_DEFINES

#include <math.h>
#include <iomanip>
#include <iostream>

int main()
{
	std::cout << std::hex << std::setiosflags(std::ios::showbase);
	std::cout << 77 << std::endl;
	std::cout << std::resetiosflags(std::ios::showbase) << 77 << std::endl;

	std::cout << std::hex << std::setiosflags(std::ios::showbase) << std::uppercase;
	std::cout << 77 << std::endl;
	std::cout << std::resetiosflags(std::ios::showbase) << std::nouppercase << 77 << std::endl;

	std::cout << std::setfill('x') << std::setw(10);
	std::cout << 10 << std::endl;
	std::cout << M_PI << "," << std::cout.precision() << std::endl;
	std::cout << std::setprecision(9) << M_PI << "," << std::cout.precision() << std::endl;

	return 0;
}