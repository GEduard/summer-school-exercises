#pragma once

#include <iostream>

// put your declarations here
class Complex
{
	double _r, _i;

	public:

		Complex();

		Complex(double a);

		Complex(double a, double b);

		void accumulate(double real);

		void accumulate(Complex complex);

		void accumulate(double real, double imag);

		double Real() const;

		double Imaginary() const;

		operator bool();

		friend double fabs(const Complex& a);

		friend Complex pow(const Complex &a, const double& power);

		friend std::ostream& operator <<(std::ostream& flux, const Complex &a);

		Complex operator +(const Complex& a);

		Complex operator +(const double d);

		Complex operator +=(const Complex& a);

		Complex operator *(const Complex& a);

		Complex operator *=(const Complex &a);

		Complex operator -(const Complex& a);

		Complex operator /(const Complex& a);

		Complex operator -=(const Complex& a);

		Complex operator /=(const Complex& a);

		Complex operator =(const Complex& a);

		bool operator ==(const Complex& a);
};
