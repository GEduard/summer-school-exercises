#include <iostream>
#include "complex.h"

double fabs(const Complex& a)
{
	return sqrt(a.Real() * a.Real() + a.Imaginary() * a.Imaginary());
}

Complex pow(const Complex &a, const double& power)
{
	double r = sqrt(a.Real() * a.Real() + a.Imaginary() * a.Imaginary());
	double t = atan2(a.Imaginary(), a.Real());
	double real = pow(r, power) * cos(power * t);
	double imaginary = pow(r, power) * sin(power * t);
	Complex complex(real, imaginary);
	return complex;
}

int main()
{
	{ // output
		Complex a;
		std::cout << a << std::endl;
	}

	{
		std::cout << "assignment between complex numbers ";
		Complex b(5);
		Complex a;
		a = b;
		if (a.Real() == 5 && a.Imaginary() == 0)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "adding complex numbers ";
		Complex a;
		Complex b(5);
		Complex c(2.5);
		a = b + c;
		if (a.Real() == 7.5 && a.Imaginary() == 0)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "subtracting complex numbers ";
		Complex a;
		Complex b(5, 3);
		Complex c(2.5, 1.5);
		a = b - c;
		if (a.Real() == 2.5 && a.Imaginary() == 1.5)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "modulus of complex numbers ";
		Complex a(3, 4);
		double modulus = fabs(a);
		if (modulus == 5)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "adding a double float precision number ";
		Complex a;
		Complex b(5);
		a = b + 3.25;
		if (a.Real() == 8.25 && a.Imaginary() == 0)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "multiplying complex numbers ";
		Complex a;
		Complex b(1, 1);
		Complex c(2, 2);
		a = b * c;

		if (a.Real() == 0 && a.Imaginary() == 4)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "dividing complex numbers ";
		Complex a;
		Complex b(1, 1);
		Complex c(2, 2);
		a = b / c;

		if (fabs(a.Real() - 0.5) < DBL_EPSILON && fabs(a.Imaginary() - 0) < DBL_EPSILON)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "accumulate a complex number ";
		Complex a;
		Complex b(1, 1);
		a += b;

		if (a.Real() == 1 && a.Imaginary() == 1)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "subtract a complex number from current number ";
		Complex a;
		Complex b(1, 1);
		a -= b;

		if (a.Real() == -1 && a.Imaginary() == -1)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "multiplying a complex number to the current number ";
		Complex a(2, 3);
		Complex b(1, 1);
		a *= b;

		if (a.Real() == -1 && a.Imaginary() == 5)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "dividing the current number by a complex number ";
		Complex a(2, 3);
		Complex b(3, 4);
		a /= b;

		if ((fabs(a.Real() - 18.0 / 25.0)) < DBL_EPSILON && fabs(a.Imaginary() - 1 / 25.0) < DBL_EPSILON)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "testing if numbers are equal ";
		Complex a(2, 3);
		Complex b(2, 3);

		if (a == b)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "raising a complex number to a real power ";
		Complex a(2, 2);

		Complex b = pow(a, 2.0);

		double realerror = fabs(b.Real() - 0);
		double imagerror = fabs(b.Imaginary() - 8);

		if(realerror <= 8 * DBL_EPSILON && 
		imagerror <= 8 * DBL_EPSILON)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}

	{
		std::cout << "Complex numbers used as booleans ";
		Complex a(2, 2);

		Complex b;

		Complex c(0, 2);

		Complex d(2, 0);

		if (a && !b && c && d)
			std::cout << "passed";
		else
			std::cout << "failed";
		std::cout << std::endl;
	}
	return 0;
}
