#include <iostream>
#include <iomanip>
#include "E:\Summer School Handout\FunctionOverloading\constructor_overloading\complex.h"

class DistanceFunctor
{
	Complex m_origin;

public:
	DistanceFunctor(Complex o) : m_origin(o) {}

	double operator()(Complex p)
	{
		double ap = p.Imaginary() - m_origin.Imaginary();
		double bp = p.Real() - m_origin.Real();
		return sqrt(pow(ap, 2) + pow(bp, 2));
	}
};

double disntace(double a, double b)
{
	return fabs(a - b);
}


void sortAcordingToDistance(Complex entriesToSort[], int count, Complex origin)
{
	DistanceFunctor d(origin);
	for (size_t i = 0; i < count - 1; i++)
		for (size_t j = i + 1; j < count; j++)
		{
			if (d(entriesToSort[i]) > d(entriesToSort[j]))
			{
				Complex aux = entriesToSort[i];
				entriesToSort[i] = entriesToSort[j];
				entriesToSort[j] = aux;
			}
		}
}

int main()
{
	DistanceFunctor d({ 2.5, 3.0 });

	Complex entries[] = { Complex(3.0, 2.5) , Complex(-1.7, 4.3), Complex(0.9, 0.0)};
	int size = sizeof(entries) / sizeof(entries[0]);

	sortAcordingToDistance(entries, size, Complex(2.5, 3.0));

	for (size_t i = 0; i < size; i++)
		std::cout << entries[i] << " - " << std::setprecision(2) << d(entries[i]) << std::endl;

	std::cout << std::endl;
	return 0;
}