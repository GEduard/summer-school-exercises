#include <iostream>
#include <iomanip>

typedef struct _Point { double x; double y; } Point;

class DistanceFunctor
{
	Point m_origin;

public:
	DistanceFunctor(Point o) : m_origin(o) {}

	double operator()(Point p)
	{
		return sqrt((m_origin.x - p.x) * (m_origin.x - p.x) + (m_origin.y - p.y) * (m_origin.y - p.y));
	}
};

double disntace(double a, double b)
{
	return fabs(a - b);
}


void sortAcordingToDistance(Point entriesToSort[], int count, Point origin)
{
	DistanceFunctor d(origin);
	for(size_t i = 0; i < count - 1; i++)
		for (size_t j = i + 1; j < count; j++)
		{
			if (d(entriesToSort[i]) > d(entriesToSort[j]))
			{
				Point aux = entriesToSort[i];
				entriesToSort[i] = entriesToSort[j];
				entriesToSort[j] = aux;
			}
		}
}

int main()
{
	DistanceFunctor d({ 2.5, 3.0 });

	Point entries[] = { {3.0, 2.5}, {-1.7, 4.3}, {0.9, 0.0} };
	int size = sizeof(entries) / sizeof(entries[0]);

	sortAcordingToDistance(entries, size, { 2.5, 3.0 });

	for (size_t i = 0; i < size; i++)
		std::cout << "{" << entries[i].x << ", " << entries[i].y << "} - " << std::setprecision(2) << d(entries[i]) << std::endl;

	std::cout << std::endl;
	return 0;
}