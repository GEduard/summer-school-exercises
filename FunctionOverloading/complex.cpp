#include "complex.h"

Complex::Complex()
{
	_r = 0;
	_i = 0;
}

Complex::Complex(double a)
{
	_r = a;
	_i = 0;
}

Complex::Complex(double a, double b)
{
	_r = a;
	_i = b;
}

void Complex::accumulate(double real, double imag)
{
	_r += real;
	_i += imag;
}

void Complex::accumulate(double real)
{
	_r += real;
}

void Complex::accumulate(Complex complex)
{
	_r += complex.Real();
	_i += complex.Imaginary();
}

// put the definitions here
double Complex::Real() const
{
	return _r;
}

double Complex::Imaginary() const
{
	return _i;
}

Complex Complex::operator +(const Complex& a)
{
	Complex complex(_r + a.Real(), _i + a.Imaginary());
	return complex;
}

Complex Complex::operator +(const double d)
{
	Complex complex(_r + d, _i);
	return complex;
}

Complex Complex::operator +=(const Complex& a)
{
	_r += a.Real();
	_i += a.Imaginary();
	return *this;
}

Complex Complex::operator *(const Complex& a)
{
	double real = _r * a.Real() - _i * a.Imaginary();
	double imaginary = _r * a.Imaginary() + a.Real() * _i;
	Complex complex(real, imaginary);
	return complex;
}

Complex Complex::operator *=(const Complex& a)
{
	*this = *this * a;
	return *this;
}

Complex Complex::operator -=(const Complex& a)
{
	_r -= a.Real();
	_i -= a.Imaginary();
	return *this;
}

Complex Complex::operator /(const Complex& a)
{
	Complex conj(a.Real(), -a.Imaginary());
	Complex complex = *this * conj;
	complex._r = complex._r / (pow(a._r, 2) + pow(a._i, 2));
	complex._i = complex._i / (pow(a._r, 2) + pow(a._i, 2));
	return complex;
}

Complex Complex::operator /=(const Complex& a)
{
	*this = *this / a;
	return *this;
}

Complex Complex::operator -(const Complex& a)
{
	Complex complex(_r - a.Real(), _i - a.Imaginary());
	return complex;
}

Complex Complex::operator =(const Complex& a)
{
	_r = a.Real();
	_i = a.Imaginary();
	return *this;
}

Complex::operator bool()
{
	return _r || _i;
}

bool Complex::operator ==(const Complex& a)
{
	return (_r == a.Real() && _i == a.Imaginary());
}

std::ostream& operator<<(std::ostream& flux, const Complex& a)
{
	flux << "Parte reala:" << a.Real() << " Parte imaginara:" << a.Imaginary();
	return flux;
}
