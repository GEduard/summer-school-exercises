#pragma once

class Vector
{
	public:
		Vector();

		Vector(double x, double y, double z);

		Vector operator +(const Vector& v);

		Vector operator -(const Vector& v);

		double operator *(const Vector& v);

		Vector operator *(const double& d);

		~Vector();

	private:
		double m_x, m_y, m_z;
};