#include "Vector.h"

Vector::Vector() {}

Vector::Vector(double x, double y, double z): m_x(x), m_y(y), m_z(z) {}

Vector Vector::operator +(const Vector& v)
{
	Vector vector(m_x + v.m_x, m_y + v.m_y, m_z + v.m_z);
	return vector;
}

Vector Vector::operator -(const Vector& v)
{
	Vector vector(m_x - v.m_x, m_y - v.m_y, m_z - v.m_z);
	return vector;
}

double Vector::operator *(const Vector& v)
{
	double result = m_x * v.m_x + m_y * v.m_y + m_z * v.m_z;
	return result;
}

Vector Vector::operator *(const double& d)
{
	Vector vector(m_x * d, m_y * d, m_z *d);
	return vector;
}

Vector::~Vector() {}
