#include <iostream>
#include "Vector.h"

int main()
{
	Vector v1(0, 0, 1);
	Vector v2(1, 0, 0);
	std::cout << v1 * v2 << std::endl;
	return 0;
}