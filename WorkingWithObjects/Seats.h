#pragma once

#include <iostream>
#include "ICapabilities.h"

class Seats: public CarPart
{
	public:
		Seats();

		~Seats();

		void printCapabilities() const override;
};

