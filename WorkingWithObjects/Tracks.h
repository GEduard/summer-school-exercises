#pragma once

#include <iostream>
#include "ICapabilities.h"

class Tracks : public CarPart
{
	public:
		Tracks();

		~Tracks();

		void printCapabilities() const override;
};

