#pragma once

#include <iostream>
#include "ICapabilities.h"

class Body: public CarPart
{
	public:
		Body();

		~Body();

		void printCapabilities() const override;
};

