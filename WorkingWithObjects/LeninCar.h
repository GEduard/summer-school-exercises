#pragma once

#include <vector>
#include "Wheels.h"
#include "Tracks.h"
#include "Engine.h"
#include "Seats.h"
#include "Body.h"

class LeninCar
{
	public:
		LeninCar();

		LeninCar(int wheels, int seats, int tracks);

		void printCapabilities() const;

		~LeninCar();

	private:

		int m_nubmerOfWheels;

		int m_NumberOfSeats;

		int m_NumberOfTracks;

		Body m_body;

		Engine m_engine;

		std::vector<Seats> m_seats;

		std::vector<Tracks> m_tracks;

		std::vector<Wheels> m_wheels;
};
