#pragma once

#include <iostream>
#include "ICapabilities.h"

class Wheels : public CarPart
{
	public:
		Wheels();

		~Wheels();

		void printCapabilities() const override;
};

