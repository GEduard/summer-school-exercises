#pragma once

#include <iostream>
#include "ICapabilities.h"

class Engine : public CarPart
{
	public:
		Engine();

		~Engine();

		void printCapabilities() const override;
};

