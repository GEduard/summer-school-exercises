#include "LeninCar.h"

LeninCar::LeninCar() {}

LeninCar::LeninCar(int wheels, int seats, int tracks)
{
	m_nubmerOfWheels = wheels;
	m_NumberOfSeats = seats;
	m_NumberOfTracks = tracks;

	for (size_t i = 0; i < m_nubmerOfWheels; i++)
	{
		Wheels wheel = Wheels();
		m_wheels.push_back(wheel);
	}

	for (size_t i = 0; i < m_NumberOfSeats; i++)
	{
		Seats seat = Seats();
		m_seats.push_back(seat);
	}

	for (size_t i = 0; i < m_NumberOfTracks; i++)
	{
		Tracks track = Tracks();
		m_tracks.push_back(track);
	}

	m_engine = Engine();
	m_body = Body();
}

LeninCar::~LeninCar() {}

void LeninCar::printCapabilities() const
{
	m_body.printCapabilities();
	m_engine.printCapabilities();
	if(m_NumberOfSeats > 0)
		m_seats.at(0).printCapabilities();

	if(m_NumberOfTracks > 0)
		m_tracks.at(0).printCapabilities();

	if(m_nubmerOfWheels > 0)
		m_wheels.at(0).printCapabilities();
}
