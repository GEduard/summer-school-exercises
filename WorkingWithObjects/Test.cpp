#include <iostream>
#include "LeninCar.h"

int main()
{
	LeninCar car = LeninCar(2, 5, 2);
	LeninCar tricycle = LeninCar(3, 2, 0);
	car.printCapabilities();
	std::cout << std::endl;
	tricycle.printCapabilities();
	std::cout << std::endl;
	std::cout << "Sizeof(LeninCar): " << sizeof(car) << " Sizeof(Tricycle): " << sizeof(tricycle) << std::endl;
	return 0;
}