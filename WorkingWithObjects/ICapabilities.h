#pragma once
class CarPart
{
	public:
		virtual void printCapabilities() const = 0;
};
