#pragma once

#include <iostream>
#include <string>

class Athlete
{
	public:
		Athlete();

		Athlete(std::string name, std::string nationality);

		virtual void Interrogate() const;

		virtual ~Athlete();

	protected:
		std::string m_name, m_nationality;
};

