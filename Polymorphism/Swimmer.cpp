#include "Swimmer.h"

Swimmer::Swimmer() {}

Swimmer::Swimmer(std::string name, std::string nationality)
{
	m_name = name;
	m_nationality = nationality;
}

void Swimmer::Interrogate() const
{
	std::cout << "My name is " << m_name << ", I am from " << m_nationality << " and I am a swimmer!" << std::endl;
}


Swimmer::~Swimmer() {}
