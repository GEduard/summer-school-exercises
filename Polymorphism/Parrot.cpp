#include "Parrot.h"

class EuropeanParrot : public Parrot
{
	public:
		EuropeanParrot() : Parrot( false) {};

		double GetSpeed() const override
		{
			return GetBaseSpeed();
		}

		Type GetType() const override
		{
			return Parrot::Type::EUROPEAN;
		}
};

class AfricanParrot : public Parrot
{
	public:
		AfricanParrot(int numberOfCoconuts) : m_numberOfCoconuts(numberOfCoconuts), Parrot(false) {}

		double GetSpeed() const override
		{
			return std::max(0., GetBaseSpeed() - GetLoadFactor() * m_numberOfCoconuts);
		}

		Type GetType() const override
		{
			return Parrot::Type::AFRICAN;
		}

	private:
		int m_numberOfCoconuts;

		double GetLoadFactor() const
		{
			return 9.;
		}
};

class NorwegianParrot : public Parrot
{
	public:
		NorwegianParrot(double voltage, bool isInjured) : m_voltage(voltage), Parrot( isInjured) {}

		double GetSpeed() const override
		{
			return m_isInjured ? 0. : GetBaseSpeed(m_voltage);
		}

		Type GetType() const override
		{
			return Parrot::Type::NORWEGIAN_BLUE;
		}

	private:
		double m_voltage;
		double GetBaseSpeed(double voltage) const
		{
			const double maxSpeed = 24.;
			return std::min(maxSpeed, voltage * Parrot::GetBaseSpeed());
		}
};

Parrot::Parrot(bool isInjured) :
   m_isInjured(isInjured)
{

}

Parrot::~Parrot() {}

double Parrot::GetBaseSpeed() const
{
    return 12.;
}

Parrot* CreateParrot(Parrot::Type type, int numberOfCoconuts, double voltage, bool isInjured)
{
	if (type == Parrot::Type::EUROPEAN) return new EuropeanParrot();
	if (type == Parrot::Type::AFRICAN) return new AfricanParrot(numberOfCoconuts);
	if (type == Parrot::Type::NORWEGIAN_BLUE) return new NorwegianParrot(voltage, isInjured);
    
	throw std::logic_error("Unrecognized type");
}
