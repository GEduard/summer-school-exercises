#pragma once
#include <algorithm>
#include <stdexcept>

class Parrot
{
public:
    enum class Type
    {
        EUROPEAN,
        AFRICAN,
        NORWEGIAN_BLUE
    };

    Parrot(bool isInjured);
	virtual ~Parrot();

    virtual double GetSpeed() const = 0;
    virtual Type GetType() const = 0;

protected:
	double GetBaseSpeed() const;

    bool m_isInjured;
};

Parrot* CreateParrot(Parrot::Type type, int numberOfCoconuts, double voltage, bool isInjured);
