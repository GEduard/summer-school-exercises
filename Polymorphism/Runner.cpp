#include "Runner.h"

Runner::Runner() {}

Runner::Runner(std::string name, std::string nationality)
{
	m_name = name;
	m_nationality = nationality;
}

void Runner::Interrogate() const
{
	std::cout << "My name is " << m_name << ", I am from " << m_nationality << " and I am a runner!" << std::endl;
}

Runner::~Runner() {}
