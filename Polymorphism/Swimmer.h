#pragma once

#include <iostream>
#include <string>
#include "Athlete.h"


class Swimmer : public Athlete
{
	public:
		Swimmer();

		Swimmer(std::string name, std::string nationality);

		void Interrogate() const;

		~Swimmer();
};

