#pragma once

#include <iostream>
#include <string>
#include "Athlete.h"

class Runner : public Athlete
{
	public:

		Runner();

		Runner(std::string name, std::string nationality);

		void Interrogate() const;

		~Runner();
};

