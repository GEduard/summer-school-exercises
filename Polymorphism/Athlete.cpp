#include "Athlete.h"

Athlete::Athlete() {}

Athlete::Athlete(std::string name, std::string nationality)
{
	m_name = name;
	m_nationality = nationality;
}

void Athlete::Interrogate() const
{
	std::cout << "I am a dummy!" << std::endl;
}

Athlete::~Athlete() {}
