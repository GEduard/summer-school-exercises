#include "Tennisman.h"

Tennisman::Tennisman() {}

Tennisman::Tennisman(std::string name, std::string nationality)
{
	m_name = name;
	m_nationality = nationality;
}

void Tennisman::Interrogate() const
{
	std::cout << "My name is " << m_name << ", I am from " << m_nationality << " and I play tennis!" << std::endl;
}

Tennisman::~Tennisman() {}
