#include "DoNotModify.h"

enum class ItemType
{
	DEFAULT,
	AGED_BRIE,
	SULFURAS,
	BACKSTAGE_PASS,
	CONJURED
};

static ItemType GetType(const Item& item)
{
	if (item.GetName() == "Aged Brie")
	{
		return ItemType::AGED_BRIE;
	}

	if (item.GetName() == "Backstage passes to a TAFKAL80ETC concert")
	{
		return ItemType::BACKSTAGE_PASS;
	}

	if (item.GetName() == "Sulfuras, Hand of Ragnaros")
	{
		return ItemType::SULFURAS;
	}

	if (item.GetName() == "Conjured Mana Cake")
	{
		return ItemType::CONJURED;
	}

	return ItemType::DEFAULT;
}

class IUpdate
{
public:
	virtual ~IUpdate() {};
	virtual void UpdateQuality(Item &item) const = 0;
	virtual void UpdateSellIn(Item &item) const = 0;
	virtual void UpdatePostSellInQuality(Item &item) const = 0;
};

class DefaultUpdate : public IUpdate
{
public:
	DefaultUpdate() = default;

	virtual void UpdateQuality(Item &item) const override
	{
		if (item.GetQuality() > 0)
		{
			item.Update(item.GetSellIn(), item.GetQuality() - 1);
		}
	}

	virtual void UpdateSellIn(Item &item) const override
	{
		item.Update(item.GetSellIn() - 1, item.GetQuality());
	}

	virtual void UpdatePostSellInQuality(Item &item) const override
	{
		UpdateQuality(item);
	}
};

class NonDefaultUpdate : public DefaultUpdate
{
public:
	NonDefaultUpdate() = default;

	virtual void UpdateQuality(Item &item) const override
	{
		if (item.GetQuality() < 50)
		{
			item.Update(item.GetSellIn(), item.GetQuality() + 1);
		}
	}
};

class BackstagePassUpdate : public NonDefaultUpdate
{
public:
	BackstagePassUpdate() = default;

	virtual void UpdateQuality(Item &item) const override
	{
		NonDefaultUpdate::UpdateQuality(item);

		if (item.GetSellIn() < 11)
		{
			NonDefaultUpdate::UpdateQuality(item);
		}

		if (item.GetSellIn() < 6)
		{
			NonDefaultUpdate::UpdateQuality(item);
		}
	}

	virtual void UpdatePostSellInQuality(Item &item) const override
	{
		item.Update(item.GetSellIn(), 0);
	}
};

class SulfurasUpdate : public NonDefaultUpdate
{
public:
	SulfurasUpdate() = default;

	virtual void UpdateSellIn(Item &item) const override
	{

	}
};

class ConjuredUpdate : public DefaultUpdate
{
public:
	ConjuredUpdate() = default;

	virtual void UpdateQuality(Item &item) const override
	{
		DefaultUpdate::UpdateQuality(item);
		DefaultUpdate::UpdateQuality(item);
	}
};

IUpdate* CreateUpdate(const Item &item)
{
	if (GetType(item) == ItemType::DEFAULT)
		return new DefaultUpdate();
	else if (GetType(item) == ItemType::BACKSTAGE_PASS)
		return new BackstagePassUpdate();
	else if (GetType(item) == ItemType::SULFURAS)
		return new SulfurasUpdate();
	else if (GetType(item) == ItemType::CONJURED)
		return new ConjuredUpdate();
	else
		return new NonDefaultUpdate();
}

void UpdateItem(Item& item)
{
	IUpdate *update = CreateUpdate(item);

	update->UpdateQuality(item);
	update->UpdateSellIn(item);
	if (item.GetSellIn() < 0)
	{
		update->UpdatePostSellInQuality(item);
	}

	delete update;
}