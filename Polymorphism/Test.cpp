#include <iostream>
#include "Runner.h"
#include "Athlete.h"
#include "Swimmer.h"
#include "Tennisman.h"

void interrogateAthletes(Athlete** athleteArray, size_t size)
{
	for (size_t i = 0; i < size; i++)
		athleteArray[i]->Interrogate();
}

int main()
{
	const int numberOfAthletes = 4;
	Athlete* swimmer = new Swimmer("Edward", "USA");
	Athlete* runner = new Runner("Remus", "England");
	Athlete* tennis1 = new Tennisman("Simona", "Romania");
	Athlete* tennis2 = new Tennisman("Otto", "Hungary");
	Athlete** athleteArray = new Athlete*[numberOfAthletes] { swimmer, runner, tennis1, tennis2 };
	interrogateAthletes(athleteArray, 4);
	for (size_t i = 0; i < numberOfAthletes; i++)
		delete athleteArray[i];
	delete[] athleteArray;
	return 0;
}