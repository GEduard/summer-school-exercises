#pragma once

#include <iostream>
#include <string>
#include "Athlete.h"

class Tennisman : public Athlete
{
	public:
		Tennisman();

		Tennisman(std::string name, std::string nationality);

		void Interrogate() const;

		~Tennisman();
};

