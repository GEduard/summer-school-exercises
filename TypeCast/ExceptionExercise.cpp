#include <iostream>
#include <chrono>
#include <string>

int f(int n)
{
	std::string obj = "Level";
	if (n == 88)
		throw "Eception";
	if (n <= 100)
		return f(n + 1);
	return 0;
}

int main()
{
	int n = 1;
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	try
	{
		f(n);
	}
	catch (...)
	{
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = end - start;
		printf("Normal return: %lf\n", elapsed_seconds.count());
	}
	return 0;
}