#include <iostream>

void sizeofint()
{
	int a = 5;
	int *poz = &a;
	int *nextPoz = poz++;

	char* fadr = reinterpret_cast<char*>(poz);
	char* sadr = reinterpret_cast<char*>(nextPoz);

	std::cout << std::abs(sadr - fadr) << std::endl;
}

int main()
{
	sizeofint();
	return 0;
}