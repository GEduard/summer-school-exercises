#include <stdio.h>

int a[ 3 ][ 3 ] = { { 1 , 2 , 3 } , { 4 , 5 , 6 } , { 7 , 8 , 9 } };

int sum( int a[ 3 ][ 3 ] )
{
  int i, j, k = 0;
  for ( i = 0; i < 3; i++ ) {
    for ( j = 0; j < 3; j++ ) {
      k += a[ i ][ j ];
    }
  }
  return k;
}

int main()
{
  printf( "The triangular sum is %d\n" , sum( a ) );
  return 0;
}
