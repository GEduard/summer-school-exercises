#include <stdio.h>

char word[] = "rotator";

bool palindrome()
{
	int i, n = (sizeof(word) / sizeof(char));
	for (i = 0;  i <= n; i++) 
	{
		if ( word[ i ] != word[ n ] ) return false;
		n--;
	}
	return true;
}

int main()
{
	printf( "%s is%s a palindrome\n" , word , palindrome() ? "" : " not" );
	return 0;
}