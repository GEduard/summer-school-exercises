enum tea
{
	cinnamon = 1
	, ginseng = 2
	, ginger = 4
	, goji = 8
	, anise = 0x010
	, licorice = 0x020
	, kava = 0x040
	, clover = 0x080
	, hawthorn = 0x100
	, tulsi = 0x200
};

typedef int Blend;
Blend StdMix(void), StdTaste(void), StdSpice(void);

Blend WakeUp = ginger | ginseng;
Blend SleepDown = kava | clover;
Blend Throaty = StdMix() | licorice;
Blend TasteGood = StdSpice() | kava;
Blend Spicy = cinnamon | anise;

Blend StdMix()
{
	return StdTaste() | hawthorn;
}

Blend StdTaste()
{
	return TasteGood | goji;
}

Blend StdSpice()
{
	return cinnamon | anise;
}

int main(void)
{
	return 0;
}