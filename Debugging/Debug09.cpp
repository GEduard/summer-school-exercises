#include <iostream>

int main()
{
	int shape = 17;
	double degree = 0.0, transition;
	transition = 100.0 / static_cast<double>(shape);
	for ( ; ; ) 
	{
		if (degree > 100.0) 
		{
			std::cout << degree << " is greater than 100.0\n";
			break;
		}
		else if (std::fabs(degree - 100.0) < FLT_EPSILON)
			std::cout << degree << " is equal to 100.0\n";
		else if (degree < 100.0)
			std::cout << degree << " is smaller than 100.0\n";
		degree += transition;
	}
	return 0;
}
