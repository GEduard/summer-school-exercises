#include <stdio.h>
#include <string.h>

void print(const char *);

int main()
{
	print("Mitt Romney");
	print("Newt Gingrich");
	print("Rick Santorum");
	print("Ron Paul");
	return 0;
}

void print(const char *name)
{
	int indent = 100 / (strlen(name)) / 2;
	for (int i = 0; i < indent; i++)
		printf(" ");
	printf("%s\n", name);
}