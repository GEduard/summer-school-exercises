#pragma once

class Rgb
{
	public:
		int m_red, m_green, m_blue;
		Rgb(int r = 255, int g = 0, int b = 0);
};