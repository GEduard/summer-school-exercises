#pragma once

#include <iostream>		

class BaseCar
{
	protected:
		int m_wheelsCount;

	public:
		BaseCar(int wheels = 4);

		int GetWheelsCount();

		void printCapabilities();

		virtual ~BaseCar();
};