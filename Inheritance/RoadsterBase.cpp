#include "RoadsterBase.h"

void RoadsterBase::printCapabilities()
{
	BaseCar::printCapabilities();
	std::cout << "I can go anywhere and carry passangers!" << std::endl;
}

RoadsterBase::~RoadsterBase()
{
	std::cout << "RoadsterBase destroyed!" << std::endl;
}

RoadsterBase::RoadsterBase(int seats, int wheels)
{
	m_seatsCount = seats;
	m_wheelsCount = wheels;
}

int RoadsterBase::GetSeatsCount()
{
	return m_seatsCount;
}