#include <iostream>

class Patrulater
{
	public:
		int numberOfSides = 4;
};

class Romb : virtual public Patrulater
{

};

class Dreptunghi : virtual public Patrulater
{

};

class Patrat : public Romb, Dreptunghi
{

};

int main()
{
	std::cout << sizeof(Patrulater) << " " 
		<< sizeof(Romb) << " " << sizeof(Dreptunghi) << " " << sizeof(Patrat) << std::endl;
	return 0;
}