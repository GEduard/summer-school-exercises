#include <iostream>
#include "Roadster.h"

void DescribeCar(BaseCar bc)
{
	std::cout << "Driving a car that has " << bc.GetWheelsCount() << " wheels!" << std::endl;
	bc.printCapabilities();
}

int main()
{
	/*BaseCar baseCar(4);
	RoadsterBase roadsterBase(4, 3);
	Roadster roadster(Rgb(255, 0, 0), 4, 2);
	DescribeCar(baseCar);
	std::cout << std::endl;
	DescribeCar(roadsterBase);
	std::cout << std::endl;
	DescribeCar(roadster);
	std::cout << std::endl;*/

	//Roadster roadster;

	BaseCar* b = new Roadster();
	delete b;

	return 0;
}