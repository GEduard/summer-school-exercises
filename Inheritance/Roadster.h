#pragma once

#include <iostream>
#include "Rgb.h"
#include "RoadsterBase.h"

class Roadster : public RoadsterBase
{
	char* nume;

	Rgb m_color;

	public:
		Roadster(Rgb color = Rgb(255, 0, 0), int seats = 4, int wheels = 4);

		Rgb GetColor();

		void MorphToTricycle();

		void printCapabilities();

		~Roadster();
};