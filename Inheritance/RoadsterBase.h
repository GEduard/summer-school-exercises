#pragma once

#include <iostream>
#include "BaseCar.h"

class RoadsterBase : public BaseCar
{
	protected:
		int m_seatsCount;

	public:
		RoadsterBase(int seats = 2, int wheels = 4);

		int GetSeatsCount();

		void printCapabilities();

		virtual ~RoadsterBase();
};