#include "BaseCar.h"

void BaseCar::printCapabilities()
{
	std::cout << "I can go down hill!" << std::endl;
}

BaseCar::~BaseCar() 
{
	std::cout << "BaseCar destroyed!" << std::endl;
}

BaseCar::BaseCar(int wheels)
{
	m_wheelsCount = wheels;
}

int BaseCar::GetWheelsCount()
{
	return m_wheelsCount;
}
