#include "Roadster.h"

Roadster::Roadster(Rgb color, int seats, int wheels)
{
	nume = new char[10];
	m_color = color;
	m_seatsCount = seats;
	m_wheelsCount = wheels;
}

Rgb Roadster::GetColor()
{
	return m_color;
}

void Roadster::MorphToTricycle()
{
	m_wheelsCount = 3;
	m_seatsCount = 1;
}

void Roadster::printCapabilities()
{
	std::cout << "I can also attract attention because I look good!" << std::endl;
}

Roadster::~Roadster()
{
	delete[] nume;
	std::cout << "Roadster destroyed!" << std::endl;
}
