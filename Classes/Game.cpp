#include "Game.h"

BlackJackGame::BlackJackGame()
{
	playerScore = 0;
	dealerScore = 0;
	cardDeck = Deck();
	cardDeck.shuffleDeck();
}

void BlackJackGame::play()
{
	//Dealing secret card
	Card normalCard;
	Card secretCard = cardDeck.dealCard();
	std::cout << "The dealer has been dealt the secret card!" << std::endl;
	dealerScore += secretCard.getCardValue();

	//Dealing first card to player
	normalCard = cardDeck.dealCard();
	playerScore += normalCard.getCardValue();
	std::cout << "The player has been dealt a card: "; 
	normalCard.printCard(); 
	std::cout << " Player score: " << playerScore << std::endl;

	//Dealing second card to dealer
	normalCard = cardDeck.dealCard();
	dealerScore += normalCard.getCardValue();
	std::cout << "The dealer has been dealt a card: ";
	normalCard.printCard();
	std::cout << std::endl;

	//Dealing second card to player
	normalCard = cardDeck.dealCard();
	playerScore += normalCard.getCardValue();
	std::cout << "The player has been dealt a card: ";
	normalCard.printCard();
	std::cout << " Player score: " << playerScore << std::endl;

	while (playerScore < maxScore)
	{
		std::cout << "Hit/Stand(0/1):";
		std::cin >> option;
		if (option == 0)
		{
			normalCard = cardDeck.dealCard();
			playerScore += normalCard.getCardValue();
			std::cout << "The player has been dealt a card: ";
			normalCard.printCard();
			std::cout << " Player score: " << playerScore << std::endl;
		}
		if (option == 1) break;
	}

	if (playerScore <= maxScore)
	{
		std::cout << "Dealer's secret card revealed: ";
		secretCard.printCard();
		std::cout << " Dealer score: " << dealerScore << std::endl;

		while (dealerScore < maxDealerScore)
		{
			normalCard = cardDeck.dealCard();
			dealerScore += normalCard.getCardValue();
			std::cout << "The dealer has been dealt a card: ";
			normalCard.printCard();
			std::cout << " Dealer score: " << dealerScore << std::endl;
		}

		if (dealerScore <= maxScore)
		{
			if (dealerScore < playerScore) std::cout << "Player wins!" << std::endl;
			if (dealerScore == playerScore) std::cout << "Draw!" << std::endl;
			if (dealerScore > playerScore) std::cout << "Dealer wins!" << std::endl;
		}
		else
		{
			std::cout << "Player wins!" << std::endl;
		}
	}
	else
	{
		std::cout << "Dealer wins!" << std::endl;
	}

}

BlackJackGame::~BlackJackGame()
{

}
