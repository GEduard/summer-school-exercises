#include "PointClass.h"

Point2D::Point2D() {}

Point2D::Point2D(Point2D&& point): m_x(point.m_x), m_y(point.m_y), m_size(point.m_size)
{
	m_array = point.m_array;
	point.m_array = nullptr;
	point.m_size = 0;
}

Point2D Point2D::operator =(Point2D&& point)
{
	m_x = point.m_x;
	m_y = point.m_y;
	m_size = point.m_size;
	m_array = point.m_array;
	point.m_size = 0;
	return *this;
}

Point2D::Point2D(const Point2D& point)
{
	m_x = point.m_x;
	m_y = point.m_y;
	m_size = point.m_size;
	m_array = new int[m_size];
	for (size_t i = 0; i < m_size; i++)
		m_array[i] = point.m_array[i];
}

Point2D Point2D::operator =(const Point2D& point)
{
	m_x = point.m_x;
	m_y = point.m_y;
	m_size = point.m_size;
	m_array = new int[m_size];
	for (size_t i = 0; i < m_size; i++)
		m_array[i] = point.m_array[i];
	return *this;
}

Point2D::Point2D(double x, double y, int size): m_x(x), m_y(y), m_size(size) 
{
	m_array = new int[m_size];
}

void Point2D::print()
{
	std::cout << "Point2D(" << m_x << ", " << m_y << ")" << std::endl;
	std::cout << "\t" << m_size << " allocated at " << m_array << std::endl;
}

double Point2D::distanceTo(const Point2D & point)
{
	return sqrt((m_x - point.m_x) * (m_x - point.m_x) + (m_y - point.m_y) * (m_y - point.m_y));
}

Point2D::~Point2D() 
{
	delete[] m_array;
}
