#pragma once

#include <string>
#include "Deck.h"

const int maxScore = 21;
const int maxDealerScore = 17;

class BlackJackGame
{
	public:
		BlackJackGame();

		void play();

		~BlackJackGame();

	private:

		Deck cardDeck;

		int option;

		int dealerScore, playerScore;
};