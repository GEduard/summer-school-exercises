#include "Card.h"

Card::Card() {}

Card::Card(Rank cRank, Suit cSuit)
{
	m_Rank = cRank;
	m_Suit = cSuit;
}

void Card::printCard() const
{
	switch (m_Rank)
	{
		case Card::Rank::TWO: std::cout << "2";
			break;
		case Card::Rank::THREE: std::cout << "3";
			break;
		case Card::Rank::FOUR: std::cout << "4";
			break;
		case Card::Rank::FIVE: std::cout << "5";
			break;
		case Card::Rank::SIX: std::cout << "6";
			break;
		case Card::Rank::SEVEN: std::cout << "7";
			break;
		case Card::Rank::EIGHT: std::cout << "8";
			break;
		case Card::Rank::NINE: std::cout << "9";
			break;
		case Card::Rank::TEN: std::cout << "10";
			break;
		case Card::Rank::ACE: std::cout << "A";
			break;
		case Card::Rank::JACK: std::cout << "J";
			break;
		case Card::Rank::QUEEN: std::cout << "Q";
			break;
		case Card::Rank::KING: std::cout << "K";
			break;
		default:
			break;
	}

	switch (m_Suit)
	{
		case Card::SPADES: std::cout << "S";
			break;
		case Card::DIAMONDS: std::cout << "D";
			break;
		case Card::HEARTS: std::cout << "H";
			break;
		case Card::CLUBS: std::cout << "C";
			break;
		default:
			break;
	}
}

int Card::getCardValue() const
{
	if (m_Rank < 10)
		return m_Rank;
	else
		return 10;
}

Card::~Card() {}
