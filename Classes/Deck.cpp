#include "Deck.h"

Deck::Deck()
{
	for(size_t i = 2; i < Card::Rank::CR_NONE; i++)
		for (size_t j = 0; j < Card::Suit::CS_NONE; j++)
		{
			Card card(static_cast<Card::Rank>(i), static_cast<Card::Suit>(j));
			cardDeck.push_back(card);
		}
}

Card Deck::dealCard()
{
	Card dealtCard = cardDeck.at(cardDeck.size() - 1);
	cardDeck.pop_back();
	return dealtCard;
}

void Deck::printDeck()
{
	for (size_t i = 0; i < deckSize; i++)
	{
		cardDeck.at(i).printCard();
		std::cout << " ";
	}
	std::cout << std::endl;
}

void Deck::shuffleDeck()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, 51);

	for(size_t i = 0; i < deckSize; i++)
	{
		int swapIndex = dis(gen);
		if (i != swapIndex)
		{
			Card auxCard = cardDeck.at(i);
			cardDeck.at(i) = cardDeck.at(swapIndex);
			cardDeck.at(swapIndex) = auxCard;
		}
	}
}

Deck::~Deck() {}
