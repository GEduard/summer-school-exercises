#pragma once

#include <iostream>

class Point2D
{
	friend Point2D duplicate(const Point2D& point);

	private:
		int* m_array = nullptr;

		int m_size = 0;

		double m_x = 0.0;

		double m_y = 0.0;

	public:
		Point2D();

		Point2D(Point2D&& point);

		Point2D(const Point2D& point);

		Point2D(double x, double y, int size);

		Point2D operator =(const Point2D &point);

		Point2D Point2D::operator =(Point2D&& point);

		void print();

		double distanceTo(const Point2D& point);

		~Point2D();
};