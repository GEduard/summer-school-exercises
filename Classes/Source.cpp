#include "PointClass.h"

Point2D duplicate(const Point2D& point)
{
	return Point2D(point.m_x, point.m_y, point.m_size);
}

int main()
{
	Point2D first(3.0, 4.0, 20);
	Point2D second(first);
	Point2D third = duplicate(first);
	Point2D fourth(std::move(third));
	first.print();
	second.print();
	third.print();
	fourth.print();
	return 0;
}