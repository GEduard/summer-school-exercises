#pragma once

#include <iostream>
#include <string>

class Card
{
	public:
		enum Rank
		{
			TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, 
			NINE, TEN, ACE, JACK, QUEEN, KING, CR_NONE
		};

		enum Suit
		{
			SPADES, DIAMONDS, HEARTS, CLUBS, CS_NONE
		};

		Card();

		Card(Rank cRank, Suit cSuit);

		void printCard() const;

		int getCardValue() const;

		~Card();

	private:
		Rank m_Rank;
		Suit m_Suit;
};