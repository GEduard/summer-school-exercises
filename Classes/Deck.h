#pragma once

#include "Card.h"
#include <random>
#include <vector>

const int deckSize = 52;

class Deck
{
	public:
		Deck();

		Card dealCard();

		void printDeck();

		void shuffleDeck();

		~Deck();

	private:
		std::vector<Card> cardDeck;
};