#include <iostream>

void CallCount()
{
	static int count = 0;
	count++;
	std::cout << count << std::endl;
}

int main()
{
	CallCount();
	CallCount();
	CallCount();
	CallCount();
	CallCount();
	CallCount();
	return 0;
}