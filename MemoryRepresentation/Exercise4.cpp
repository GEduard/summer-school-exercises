#include <iostream>

int main()
{
	int intValue = 10;
	int* pointerToInt = &intValue;
	int** pointerToPointerToInt = &pointerToInt;

	pointerToPointerToInt = nullptr;
	pointerToPointerToInt[0] = nullptr;
	pointerToPointerToInt[0][0] = 5;
	*pointerToPointerToInt = nullptr;
	*pointerToPointerToInt[0] = 6;
	**pointerToPointerToInt = 7;
	return 0;
}