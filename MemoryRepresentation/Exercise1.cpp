#include <iostream>

int main()
{
	float a, b, c, p;
	std::cout << "Enter first number: ";
	std::cin >> a;
	std::cout << "Enter the second number: ";
	std::cin >> b;
	std::cout << "What is the expected value for the product: ";
	std::cin >> c;
	p = a * b; 
	std::cout << "Computed product: " << p << " Expected product: " << c << std::endl << "Expected == Computed : ";
	if (std::abs(p - c) < FLT_EPSILON)
		std::cout << "true" << std::endl;
	else
		std::cout << "false" << std::endl;
	return 0;
}