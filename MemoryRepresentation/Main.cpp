#include <iostream>
#include <vector>
#include "E:\Summer School Handout\Pointers\Ex_FunctionPointers.h"

int main()
{
	int n, x;
	std::vector<int> a;
	std::cout << "Introduceti numarul de elemente:";
	std::cin >> n;
	std::cout << "Introduceti elementele:";
	for (size_t i = 0; i < n; i++)
	{
		std::cin >> x;
		a.push_back(x);
	}
	for (int i = 0; i < a.size() - 1; i++)
	{
		for (int j = i + 1; j < a.size(); j++)
		{
			if (Ex::Compare(a[i], a[j], Ex::SortAscending))
			{
				int aux = a[i];
				a[i] = a[j];
				a[j] = aux;
			}
		}
	}
	std::cout << "Elementele sunt:";
	for (size_t i = 0; i < a.size(); i++)
		std::cout << a[i] << " ";
	std::cout << std::endl;
	return 0;
}