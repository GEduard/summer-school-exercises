#include <iostream>
#include "Ex_FunctionPointers.h"

namespace
{
    void PrintInt(int i)
    {
        std::cout<<i<<"\n";
    }
    // function as function parameter
    void CallFunc(void (*p)(int), int i)
    {
        p(i);    
    }
}

namespace Ex
{
	bool Compare(int a, int b, Comparator comparator)
	{
		return comparator(a, b);
	}

	void PointerToFunction()
    {
        void (*p)(int) = PrintInt; // function pointer declaration and initialization
        p(3);
        p(4);
        CallFunc(p,33);
        CallFunc(PrintInt,44);
    }

	double printDouble(double a, double b, int option)
	{
		switch (option)
		{
			case 0: return -1;
			case 1: return a + b;
			case 2: return a - b;
			case 3: return a * b;
			case 4: return fabs(a - b);
		}
	}

    // Declare Operation as a function that takes two double parameters and returns a double;
    typedef double (*Operation)(double, double, int); 

	typedef bool(*Comparator)(int a, int b);

    // Implement a function that receives an Operation as input and return the result applied on param1 and param2
    double GetResult(Operation operation, double param1, double param2, int option)
    {
		return operation(param1, param2, option);
    }

	bool SortAscending(int a, int b)
	{
		return a > b;
	}
}
