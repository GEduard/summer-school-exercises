#pragma once

#include <vector>

namespace Ex
{
	void PointerToFunction();

	bool SortAscending(int a, int b);

	double printDouble(double a, double b, int option);

    // Declare Operation as a function that takes two double parameters and returns a double;
    typedef double (*Operation)(double, double, int); 

	typedef bool(*Comparator)(int a, int b);

	bool Compare(int a, int b, Comparator comparator);

    // Implement a function that receives an Operation as input and return the result applied on param1 and param2
    double GetResult(Operation operation, double param1, double param2, int option);
}
