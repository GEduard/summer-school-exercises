#include <iostream>

int main()
{
	const int numFingers = 5;
	int apples = 5;

	const int& refNumFingers = numFingers;
	const int& refApples = refApples;

	const_cast<int&>(refNumFingers) = 6;
	const_cast<int&>(refApples) = 6;

	std::cout << numFingers << std::endl;
	std::cout << apples << std::endl;
	return 0;
}