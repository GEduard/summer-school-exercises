#include <iostream>


int main()
{
	int a = 1;
	int* p = &a;
	char* c = reinterpret_cast<char*>(p);
	if(*c == 0)
		std::cout << "Big Endian" << std::endl;
	else
		std::cout << "Little Endian" << std::endl;
	return 0;
}