#pragma once

#include <queue>
#include <mutex>
#include <string>
#include <memory>
#include <condition_variable>

class Storage
{
	public:
		Storage(size_t _max_size) : max_size(_max_size) {}
		auto Store(std::string&& s) -> void
		{
			std::unique_lock<std::mutex> lockGuard(mtx);
			cond_var.wait(lockGuard, [this]() {
				return data.size() < max_size;
			});
			data.push(std::move(s));
			cond_var.notify_one();
		}

		auto check_and_get(std::string& outData) -> void
		{
			std::unique_lock<std::mutex> lockGuard(mtx);
			cond_var.wait(lockGuard, [this]() 
			{
				return data.size() > 0;
			});
			outData = data.front();
			data.pop();
		}

	private:
		std::mutex mtx;
		unsigned int max_size;
		std::queue<std::string> data;
		std::condition_variable cond_var;
};

typedef std::shared_ptr<Storage> XStorage;