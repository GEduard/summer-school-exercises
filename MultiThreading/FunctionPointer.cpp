#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include <sstream>

#include "Sum.h"

auto main() -> int
{
	const auto proc_count = std::thread::hardware_concurrency();
	const auto threadFunc = [](const std::string& name, XDoubleSum sum) -> void
	{
		DoubleSum localSum;
		for (size_t i = 0; i < 100000; ++i)
		{
			localSum += 1.0;
		}
		(*sum) += localSum;
	};
	XDoubleSum sharedDoubleSum = std::make_shared<DoubleSum>();
	std::vector<std::thread> vecTh(proc_count - 1);
	for (size_t i = 0; i < proc_count - 1; ++i)
	{
		std::stringstream sstr;
		sstr << "Thread " << (i + 1);
		vecTh[i] = std::thread(threadFunc, sstr.str(), sharedDoubleSum);
	}

	for (size_t i = 0; i < proc_count - 1; ++i)
	{
		vecTh.at(i).join();
	}
	threadFunc("Main", sharedDoubleSum);
	std::cout << "Result is " << *sharedDoubleSum << std::endl;
	return 0;
}