#pragma once

#include <chrono>
#include <thread>
#include <sstream>
#include <iostream>

#include "Storage.h"

class Producer
{
	public:
		Producer(XStorage _storage) : sharedStorage(_storage) {}

		~Producer()
		{
			if(th.joinable())
				th.join();
		}

		auto launch_production() -> void
		{
			th = std::thread(&Producer::produce, this);
		}

	private:
		std::thread th;
		XStorage sharedStorage;

		auto produce() -> void
		{
			for (size_t i = 0;  ; ++i)
			{
				std::stringstream ss;
				ss << "Data " << (i+1) << " produced by " << std::this_thread::get_id();
				sharedStorage->Store(ss.str());
				std::cout << "Producer " << std::this_thread::get_id() << " ## " << ss.str() << std::endl;
			}
		}
};