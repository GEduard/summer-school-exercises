#include <iostream>
#include <future>
#include <random>
#include <vector>

auto PointsInCircle(size_t points) -> double
{
	double x, y, aproxPi;
	unsigned int pointCount = 0;
	std::random_device randDev;
	std::default_random_engine randEng(randDev());
	std::uniform_real_distribution<> randVal(-1.0, 1.0);
	for (size_t i = 0; i < points; i++)
	{
		x = randVal(randEng);
		y = randVal(randEng);
		if (pow(x, 2) + pow(y, 2) <= 1)
			pointCount++;
	}
	aproxPi = (double)(4 * pointCount) / points;
	return aproxPi;
}

auto main() -> int
{
	int points;
	double piAproximation = 0.0;
	std::vector<std::future<double>> future;
	std::cout << "Introduceti numarul de puncte: ";
	std::cin >> points;
	for (size_t i = 0; i < 4; ++i)
		future.push_back(std::async(PointsInCircle, points));
	for (auto& pi : future)
		piAproximation += pi.get();
	piAproximation /= future.size();
	std::cout << "Aproxiamtion PI: " << piAproximation << std::endl;
	return 0;
}