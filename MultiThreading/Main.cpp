#include <iostream>
#include <vector>

#include "Prod.h"
#include "Cons.h"
#include "SyncCout.h"

auto main() -> int
{
	std::vector<Producer> vecProd;
	XStorage xs = std::make_shared<Storage>(10);
	Producer p(xs);
	Consumer c(xs);
	p.launch_production();
	c.launch_consumption();
	return 0;
}