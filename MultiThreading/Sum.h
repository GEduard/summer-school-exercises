#pragma once

#include <memory>
#include <mutex>

template<typename T>
struct TSum
{
	TSum(): val(T()) {}

	TSum(const T& that) : val(that.val) {}

	TSum(const T&& that) : val(std::move(that.val)) {}

	TSum& operator += (const T& _val)
	{
		std::unique_lock<std::mutex> uLock(mtx);
		val += _val;
		return *this;
	}

	TSum& operator = (const TSum& that)
	{
		val = that.val;
		return *this;
	}

	operator T() const
	{
		return val;
	}

	private:
		T val;
		std::mutex mtx;
};

typedef TSum<double> DoubleSum;
typedef std::shared_ptr<DoubleSum> XDoubleSum;