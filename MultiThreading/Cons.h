#pragma once

#include <chrono>
#include <thread>
#include <sstream>
#include <iostream>

#include "Storage.h"

class Consumer
{
public:
	Consumer(XStorage _storage) : sharedStorage(_storage) {}

	~Consumer()
	{
		if (th.joinable())
			th.join();
	}

	auto launch_consumption() -> void
	{
		th = std::thread(&Consumer::consume, this);
	}

private:
	std::thread th;
	XStorage sharedStorage;

	auto consume() -> void
	{
		for (;;)
		{
			std::string res;
			sharedStorage->check_and_get(res);
			std::cout << "Consumer " << std::this_thread::get_id() <<
				" ## Data consumed by " << std::this_thread::get_id() << std::endl;
		}
	}
};