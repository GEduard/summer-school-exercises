#include <Amount.h>

#include <iomanip>

Amount::Amount(const int dollars, const int cents)
	: dollars(dollars), cents(cents) { }

auto operator<<(std::ostream& os, const Amount& amount) -> std::ostream& {
	os << "$" << amount.dollars << "." << std::setw(2) << std::setfill('0') << amount.cents;
	return os;
}

auto Amount::operator +(const Amount& y)const -> Amount
{
	int newDollars = this->dollars + y.dollars + (cents + y.cents) / 100;
	int newCents = (this->cents + y.cents) % 100;
	return Amount(newDollars, newCents);
}
