#include "Ex_Addresses.h"
#include <iostream>

namespace Ex
{
    void VisualizeAddressesInMemory()
    {
        auto a = 5;
        auto b = 5.0;
        const auto cb = 5.0;
        auto c = 6L;
        auto d = 7U;
        auto e = 'a';
        auto f = "str";

		std::cout << a << " " << &a << std::endl;
		std::cout << b << " " << &b << std::endl;
		std::cout << c << " " << &c << std::endl;
		std::cout << d << " " << &d << std::endl;
		std::cout << e << " " << &e << std::endl;
		std::cout << f << " " << &f << std::endl;
		std::cout << cb << " " << &cb << std::endl;
		

        // 1.0 What are the addresses of the previous declared variables? (using VS Debug)
        // 1.1 Run the program multiple times, do their address change ? Why ?

        // 2. What are the sizes of the variables previusly declared ?

        // 3. What are the sizes of their addresses ?

        // 4. Write a function that display <variable_value> : <variable_address> and use it to display information for previously declared variables

        // 5. Change target platform for compiling. What is the difference between x86 & x64?

        // 6.0 Swap the contents of two integer variables.
        // 6.1 Write a function that does that.

    }

	void swap(int &a, int &b)
	{
		int aux = a;
		a = b;
		b = aux;
	}
}