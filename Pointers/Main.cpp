#include <iostream>
#include "E:\Siemens Summer School\Pointers\Ex_Addresses.h"

int main()
{
	int a, b;
	Ex::VisualizeAddressesInMemory();
	std::cout << "Enter the first value:";
	std::cin >> a;
	std::cout << "Enter the second value:";
	std::cin >> b;
	std::cout << "The numbers are: " << a << " " << b;
	Ex::swap(a, b);
	std::cout << "\nThe numbers are: " << a << " " << b << std::endl;
	return 0;
}
